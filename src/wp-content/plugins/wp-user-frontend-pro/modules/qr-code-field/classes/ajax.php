<?php

/**
 *  Ajax functionality Class
 */
class WPUF_Ajax_QR_Code {

    /**
     * Constructor for the WPUF_QR_Code class
     *
     * Sets up all the appropriate hooks and actions
     * within our plugin.
     *
     * @uses add_action()
     */
    public function __construct() {
        // Load ajax script
        add_action( 'wp_ajax_build_qr_type_field', array( $this, 'get_qr_type_param_form' ));
    }

    /**
     * Render custom type param field when user change type in frontend
     * @return json
     */
    function get_qr_type_param_form () {
        $type = $_POST['type'];
        $postid = $_POST['postid'];
        $formfield = $_POST['formfield'];

        if( $postid ) {
            $selected = get_post_meta( $postid, $formfield, false );
            $value = $selected[0]['type_param'];
        }

        ob_start();

        switch ( $type ) {

            case 'url':
                ?>
                <p><input type="url" data-type="url" data-required="<?php echo $form_field['required'] ?>" name="<?php echo $formfield; ?>[type_param][url]" value="<?php echo ( $value['url'] ) ? $value['url'] : ''; ?>" placeholder="Enter Url" size="40"></p>
                <?php
                break;
            case 'text':
                ?>
                <p><input type="text" data-type="text" data-required="<?php echo $form_field['required'] ?>" name="<?php echo $formfield; ?>[type_param][text]" value="<?php echo ( $value['text'] ) ? $value['text'] : ''; ?>" placeholder="Enter Text" size="40"></p>
                <?php
                break;

            case 'geo':
                ?>
                <p><input type="text" data-type="text" data-required="<?php echo $form_field['required'] ?>" name="<?php echo $formfield; ?>[type_param][geo_lat]" value="<?php echo ( $value['geo_lat'] ) ? $value['geo_lat'] : ''; ?>" placeholder="Enter Geo Latitude" size="40"></p>
                <p><input type="text" data-type="text" data-required="<?php echo $form_field['required'] ?>" name="<?php echo $formfield; ?>[type_param][geo_long]" value="<?php echo ( $value['geo_long'] ) ? $value['geo_long'] : ''; ?>" placeholder="Enter Geo longitude" size="40"></p>
                <?php
                break;

            case 'sms':
                ?>
                <p><input type="text" data-type="text" data-required="<?php echo $form_field['required'] ?>" name="<?php echo $formfield; ?>[type_param][sms_tel]" value="<?php echo ( $value['sms_tel'] ) ? $value['sms_tel'] : ''; ?>" placeholder="Enter Phone number" size="40"></p>
                <p><textarea data-type="textarea" data-required="<?php echo $form_field['required'] ?>" name="<?php echo $formfield; ?>[type_param][sms_message]" id="" cols="30" rows="6" placeholder="Enter Text message"><?php echo ( $value['sms_message'] ) ? $value['url'] : ''; ?></textarea></p>
                <?php
                break;

            case 'wifi':
                ?>
                <p><input type="text" data-type="text" data-required="<?php echo $form_field['required'] ?>" name="<?php echo $formfield; ?>[type_param][wifi_type]" value="<?php echo ( $value['wifi_type'] ) ? $value['wifi_type'] : ''; ?>" placeholder="WPA, WEP or nopass" size="40"></p>
                <p><input type="text" data-type="text" data-required="<?php echo $form_field['required'] ?>" name="<?php echo $formfield; ?>[type_param][wifi_ssid]" value="<?php echo ( $value['wifi_ssid'] ) ? $value['wifi_ssid'] : ''; ?>" placeholder="SSID of the Wi-Fi network" size="40"></p>
                <p><input type="text" data-type="text" data-required="<?php echo $form_field['required'] ?>" name="<?php echo $formfield; ?>[type_param][wifi_password]" value="<?php echo ( $value['wifi_password'] ) ? $value['wifi_password'] : ''; ?>" placeholder="Password of the network" size="40"></p>
                <?php
                break;

            case 'card':
                ?>
                <p><input type="text" data-type="text" data-required="<?php echo $form_field['required'] ?>" name="<?php echo $formfield; ?>[type_param][card_name]" value="<?php echo ( $value['card_name'] ) ? $value['card_name'] : ''; ?>" placeholder="Full name" size="40"></p>
                <p><input type="text" data-type="text" data-required="<?php echo $form_field['required'] ?>" name="<?php echo $formfield; ?>[type_param][card_firm]" value="<?php echo ( $value['card_firm'] ) ? $value['card_firm'] : ''; ?>" placeholder="Firm name" size="40"></p>
                <p><input type="phone" data-type="text" data-required="<?php echo $form_field['required'] ?>" name="<?php echo $formfield; ?>[type_param][card_tel]" value="<?php echo ( $value['card_tel'] ) ? $value['card_tel'] : ''; ?>" placeholder="Phone number" size="40"></p>
                <p><input type="email" data-type="email" data-required="<?php echo $form_field['required'] ?>" name="<?php echo $formfield; ?>[type_param][card_email]" value="<?php echo ( $value['card_email'] ) ? $value['card_email'] : ''; ?>" placeholder="Email" size="40"></p>
                <p><input type="text" data-type="text" data-required="<?php echo $form_field['required'] ?>" name="<?php echo $formfield; ?>[type_param][card_address]" value="<?php echo ( $value['card_address'] ) ? $value['card_address'] : ''; ?>" placeholder="Postal address" size="40"></p>
                <p><input type="url" data-type="url" data-required="<?php echo $form_field['required'] ?>" name="<?php echo $formfield; ?>[type_param][card_url]" value="<?php echo ( $value['card_url'] ) ? $value['card_url'] : ''; ?>" placeholder="Website URL" size="40"></p>
                <p><input type="text" data-type="text" data-required="<?php echo $form_field['required'] ?>" name="<?php echo $formfield; ?>[type_param][card_memo]" value="<?php echo ( $value['card_memo'] ) ? $value['card_memo'] : ''; ?>" placeholder="More infos about the contact" size="40"></p>
                <?php
                break;

            case 'email':
                ?>
                <p><input type="email" data-type="email" data-required="<?php echo $form_field['required'] ?>" name="<?php echo $formfield; ?>[type_param][email_address]" value="<?php echo ( $value['email_address'] ) ? $value['email_address'] : ''; ?>" placeholder="Email Address" size="40"></p>
                <p><input type="text" data-type="text" data-required="<?php echo $form_field['required'] ?>" name="<?php echo $formfield; ?>[type_param][email_subject]" value="<?php echo ( $value['email_subject'] ) ? $value['email_subject'] : ''; ?>" placeholder="Subject of the email to send" size="40"></p>
                <p><textarea data-required="<?php echo $form_field['required'] ?>" data-type="text" name="<?php echo $formfield; ?>[type_param][email_message]" placeholder="Content of the email" ><?php echo ( $value['email_message'] ) ? $value['email_message'] : ''; ?></textarea>
                </p>
                <?php
                break;

            case 'calendar':
                ?>
                <p><input type="text" data-type="text" data-required="<?php echo $form_field['required'] ?>" name="<?php echo $formfield; ?>[type_param][calendar_title]" value="<?php echo ( $value['calendar_title'] ) ? $value['calendar_title'] : ''; ?>" placeholder="Name of the event" size="40"></p>
                <p><input type="text" data-type="text" data-required="<?php echo $form_field['required'] ?>" name="<?php echo $formfield; ?>[type_param][calendar_place]" value="<?php echo ( $value['calendar_place'] ) ? $value['calendar_place'] : ''; ?>" placeholder="Place of the event" size="40"></p>
                <p><input type="text" data-type="text" data-required="<?php echo $form_field['required'] ?>" name="<?php echo $formfield; ?>[type_param][calendar_begin]" value="<?php echo ( $value['calendar_begin'] ) ? $value['calendar_begin'] : ''; ?>" placeholder="Beginning event(dd/mm/yyyy hh:mm)" size="40"></p>
                <p><input type="text" data-type="text" data-required="<?php echo $form_field['required'] ?>" name="<?php echo $formfield; ?>[type_param][calendar_end]" value="<?php echo ( $value['calendar_end'] ) ? $value['calendar_end'] : ''; ?>" placeholder="End event(dd/mm/yyyy hh:mm)" size="40"></p>
                <?php
                break;

            case 'phone':
                ?>
                <p><input type="phone" data-type="text" data-required="<?php echo isset( $form_field['required'] ) ? $form_field['required'] : '' ?>" name="<?php echo $formfield; ?>[type_param][phone]" value="<?php echo isset( $value['phone'] ) ? $value['phone'] : ''; ?>" placeholder="Phone number to call" size="40"></p>
                <?php
                break;

            default:

                break;
        }

        wp_send_json_success( array( 'appned_data'=> ob_get_clean() ) );
    }
}