<div class="wpuf-qr-code">
	<select>
		<option><?php _e( '- Select -', 'wpuf-qr-code' ); ?></option>
		<option v-for="type in field.qr_type">{{ type }}</option>
	</select>
</div>