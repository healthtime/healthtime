<?php
/**
  Plugin Name: User Activity
  Plugin URI: https://wedevs.com/products/plugins/wp-user-frontend-pro/user-activity/
  Thumbnail Name: wpuf-activity.png
  Description: Handle user activity in frontend
  Version: 1.0.0
  Author: weDevs
  Author URI: https://wedevs.com
  License: GPL2
 */

/**
 * Copyright (c) 2018 wedevs (email: info@wedevs.com). All rights reserved.
 *
 * Released under the GPL license
 * http://www.opensource.org/licenses/gpl-license.php
 *
 * This is an add-on for WordPress
 * http://wordpress.org/
 *
 * **********************************************************************
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * **********************************************************************
 */



/**
 * User Activity class for WP User Frontend PRO
 *
 * @author weDevs <info@wedevs.com>
 */
class WPUF_User_Activity {

    public function __construct() {

        if ( ! current_user_can( 'manage_options' ) ) {
            return;
        }

        add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
        // add_filter( 'wpuf_ud_nav_urls', array( $this, 'ud_profile_top_navbar' ), 11 );
        add_action( 'wpuf_ud_profile_sections', array( $this, 'user_directory_profile_sections' ) );
        add_action( 'wpuf_ud_profile_activity', array( $this, 'show_activity_section' ) );

        add_action( 'init', array( $this, 'init_hooks' ) );
        add_action( 'save_post', array( $this, 'monitor_form_posts' ), 10, 3 );
        add_action( 'admin_notices', array( $this, 'wpuf_ud_module_notice' ) );
    }

    public function init_hooks() {
        add_action( 'publish_post', array( $this, 'track_activity_new_post' ), 10, 2 );
        add_action( 'post_updated', array( $this, 'check_update_post' ), 10, 3 );
        add_action( 'trash_post', array( $this, 'track_activity_trash_post' ) );
        add_action( 'delete_post', array( $this, 'track_activity_delete_post' ) );
        add_action( 'wp_insert_comment', array( $this, 'track_activity_comment_post' ), 99, 2 );
    }

    /**
     * Show admin notice when User Directory Module is not active
     *
     * @return void
     */

    function wpuf_ud_module_notice() {
        if ( ! class_exists('WPUF_User_Listing' ) ) {
        ?>
            <div class="notice notice-success is-dismissible">
                <p><?php _e( 'Please Activate User Directory module to use User Activity module.', 'wpuf-pro' ); ?></p>
            </div>
        <?php
        }
    }

    /**
     * Get curret post author
     *
     * @return mixed
     */
    public function get_profile_url() {
        $user     = wp_get_current_user();
        return get_author_posts_url( $user->ID );
    }

    /**
     * New Published post handler
     *
     * @return void
     */
    public function track_activity_new_post( $post_id, $post ) {
        global $post;


        if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) {
            return;
        }

        if ( wp_is_post_revision( $post_id ) ) {
            return;
        }

        if ( in_array( $post->post_type, array( 'nav_menu_item' ) ) ) {
            return;
        }

        if ( empty( $title ) || $post->post_title == 'Auto Draft' ) {
            return;
        }

        $user     = wp_get_current_user();
        $user_url = $this->get_profile_url();
        $title    = $post->post_title;

        if ( isset( $post_id ) && $post->post_status != 'publish' ) {
            $message = sprintf( __( 'published a post', 'wpuf-pro' ) );
            $this->log_activity( $post_id, 'post', $message );
            return;
        }

    }

    /**
     * Updated post handler
     *
     * @return void
     */
    public function check_update_post( $post_id, $post_after, $post_before ) {
        global $post;

        if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) {
            return;
        }

        if ( wp_is_post_revision( $post_id ) ) {
            return;
        }

        if ( in_array( $post->post_type, array( 'nav_menu_item' ) ) ) {
            return;
        }

        $user     = wp_get_current_user();
        $user_url = $this->get_profile_url();
        $title    = $post->post_title;

        if ( ( $post_after !== $post_before && $post->post_status != 'draft') && $post_after->post_status != 'trash' ) {
            $message = sprintf( __( 'updated a post', 'wpuf-pro' ) );
            $this->log_activity( $post_id, 'post', $message );
            remove_action( 'save_post', array( $this, 'monitor_form_posts' ), 10, 3 );
            return;
        }
    }

    /**
     * Trashed post handler
     *
     * @return void
     */
    public function track_activity_trash_post() {
        global $post;


        if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) {
            return;
        }

        if ( wp_is_post_revision( $post->ID ) ) {
            return;
        }

        if ( in_array( $post->post_type, array( 'nav_menu_item' ) ) ) {
            return;
        }

        $user     = wp_get_current_user();
        $user_url = $this->get_profile_url();
        $title    = $post->post_title;

        $message = sprintf( __( 'trashed a post', 'wpuf-pro' ) );
        $this->log_activity( $post->ID, 'post', $message );
    }

    /**
     * Deleted post handler
     *
     * @return void
     */
    public function track_activity_delete_post() {
        global $post;

        if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) {
            return;
        }

        if ( wp_is_post_revision( $post->ID ) ) {
            return;
        }

        if ( in_array( $post->post_type, array( 'nav_menu_item' ) ) ) {
            return;
        }

        $user     = wp_get_current_user();
        $user_url = $this->get_profile_url();

        $message = sprintf( __( 'deleted a post', 'wpuf-pro' ) );

        $this->log_activity( $post->ID, 'post', $message );

        if ( did_action( 'delete_post' ) > 0 ) {
            remove_action( 'delete_post', array( $this, 'track_activity_delete_post' ) );
        }
    }

    /**
     * New Comment handler
     *
     * @return void
     */
    public function track_activity_comment_post( $comment_id, $comment_object ) {

        $user     = wp_get_current_user();
        $user_url = $this->get_profile_url();
        $post_id  = $comment_object->comment_post_ID;

        $message = sprintf( __( 'commented on a post', 'wpuf-pro' ), $user_url, $user->display_name, get_permalink( $post->ID ), get_the_title( $post_id ) );

        $this->log_activity( $post_id, 'comment', $message );
    }

    /**
     * Form Posts handler
     *
     * @return void
     */
    public function monitor_form_posts( $post_id, $post, $update ) {
        global $post;

        $user  = wp_get_current_user();
        $post_author = $user->display_name;

        if ( defined('DOING_AJAX') && DOING_AJAX && $_POST['action'] === 'wpuf_submit_post' && current_user_can('manage_options') ) {
            check_ajax_referer( 'wpuf_form_add' );

            if ( !$update ) {
                $message = sprintf( __( 'published a post', 'wpuf-pro' ) );
                $this->log_activity(  $post_id, 'post', $message );
                return;
            }
        }
    }

    /**
     * Logs activity messages in database
     *
     * @return void
     */
    public function log_activity( $activity_id, $activity_type, $message ) {
        global $wpdb;

        $user    = wp_get_current_user();
        $time    = current_time( 'mysql' );

        $tablename = $wpdb->prefix . 'wpuf_activity';

        $data = array(
            'user_id'       => $user->ID,
            'activity_type' => $activity_type,
            'activity_id'   => $activity_id,
            'user_name'     => $user->display_name,
            'message'       => $message,
            'activity_time' => $time,
            'ip'            => preg_replace( '/[^0-9a-fA-F:., ]/', '', $_SERVER['REMOTE_ADDR'] )
        );

        $wpdb->insert( $tablename, $data);

    }

    /**
     * Enqueue Scripts and Styles
     *
     * @return void
     */
    public function enqueue_scripts() {
        wp_enqueue_style( 'wpuf-user-activity', plugins_url( 'css/user-activity.css', __FILE__ ) );
        wp_enqueue_script( 'wpuf-user-activity', plugins_url( 'js/user-activity.js', __FILE__ ) );
    }

    /**
     * Show Profile Tabs
     *
     * @return void
     */
    function user_directory_profile_sections( $nav_urls ) {
        global $wpdb;

        $nav_urls = apply_filters( 'wpuf_ud_nav_urls', $nav_urls );

        $user    = wp_get_current_user();
        $table   = $wpdb->prefix . 'wpuf_activity';
        $sql     = "SELECT * FROM $table WHERE user_id={$user->ID}";
        $results = $wpdb->get_results($sql);
        $timeline_date = '';
        ?>

        <div class="wpuf-profile-container">
            <ul class="wpuf-profile-tabs">
                <li class="tab-link current" data-tab="about-tab">About</li>
                <li class="tab-link" data-tab="activity-tab">Activity</li>
            </ul>

            <div id="about-tab" class="wpuf-profile-tab-content current">
                <?php do_action( 'wpuf_ud_profile_about' );  ?>
            </div>
            <div id="activity-tab" class="wpuf-profile-tab-content">
                <div class="wpuf-activity-table" >
                <?php foreach ( $results as $result ): ?>
                    <div class="wpuf-activity-row">
                        <?php
                            if ( $timeline_date != date('d-M', strtotime( $result->activity_time ) ) ) {
                                $timeline_date =  date('d-M', strtotime( $result->activity_time ) );
                                echo '<div class="wpuf-activity-date">';
                                echo $timeline_date;
                                echo '</div>';
                            }

                            $message = $this->get_activity_message( $result->user_id, $result->user_name, $result->activity_id, $result->activity_type, $result->message );

                            if ( $timeline_date == date('d-M', strtotime( $result->activity_time ) ) ) { ?>
                                <div class="wpuf-activity-column"><?php echo date('h:i A', strtotime( $result->activity_time ) ); ?></div>
                                <div class="wpuf-activity-column"><?php echo $message; ?></div>
                        <?php } ?>
                    </div>
                <?php endforeach ?>
                </div>
            </div>
        </div>

        <?php

    }

    /**
     * Create database table
     *
     * @return void
     */
    public function activation() {
        global $wpdb;

        $table_name = $wpdb->prefix . 'wpuf_activity';
        $wpdb_collate = $wpdb->collate;
        $sql =
        "CREATE TABLE {$table_name} (
        id int(11) unsigned NOT NULL auto_increment,
        user_id varchar(11) NOT NULL,
        activity_id varchar(11) NOT NULL,
        activity_type varchar(20) NOT NULL,
        user_name varchar(191) NOT NULL,
        message varchar(255) NOT NULL,
        activity_time datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
        ip varchar(39) NOT NULL,
        PRIMARY KEY  (id)
        )
        COLLATE {$wpdb_collate}";

        if ( $wpdb->get_var( "SHOW TABLES LIKE '$table_name'" ) != $table_name) {
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta( $sql );
        }
    }

    /**
     * Return Activity message
     *
     * @return string
     */
    function get_activity_message( $user_id, $user_name, $activity_id, $activity_type, $message ) {

        $message = sprintf( __( '<a href="%s">%s</a> %s <a href="%s">%s</a>.', 'wpuf-pro' ), get_author_posts_url( $user_id ), $user_name , $message, get_permalink( $activity_id ), get_the_title( $activity_id ) );

        return $message;
    }

}

/**
 * Return the instance
 *
 * @return \WPUF_User_Activity
 */
function wpuf_user_activity() {
    if ( !class_exists( 'WP_User_Frontend' ) ) {
        return;
    }

    new WPUF_User_Activity();

    wpuf_register_activation_hook( __FILE__, array( 'WPUF_User_Activity', 'activation' ) );
}

wpuf_user_activity();
