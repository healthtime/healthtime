<?php

/*  ----------------------------------------------------------------------------
    The author box - used only on the author.php template ( ! this is not the same box as the one used in the single post template ! )
 */

global $part_cur_auth_obj;



?>
<div class="author-box-wrap td-author-page">

    <?php  echo get_avatar($part_cur_auth_obj->user_email, '96'); ?>
    <div class="desc">


        <div class="td-author-counters">
            <span class="td-author-post-count">
                <?php echo count_user_posts($part_cur_auth_obj->ID). ' '  . __td('POSTS', TD_THEME_NAME);?>
            </span>

            <span class="td-author-comments-count">
                <?php
                $comment_count = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) AS total FROM $wpdb->comments WHERE comment_approved = 1 AND user_id = %d", $part_cur_auth_obj->ID));

                echo $comment_count . ' '  . __td('COMMENTS', TD_THEME_NAME);
                ?>
            </span>

        </div>

        <?php
        if (!empty($part_cur_auth_obj->user_url)) {
            echo '<div class="td-author-url"><a href="' . esc_url($part_cur_auth_obj->user_url) . '">' . esc_url($part_cur_auth_obj->user_url) . '</a></div>';
        }

        echo $part_cur_auth_obj->description;

        ?>



        <div class="td-author-social">
            <?php

            foreach (td_social_icons::$td_social_icons_array as $td_social_id => $td_social_name) {
                //echo get_the_author_meta($td_social_id) . '<br>';
                $authorMeta = get_the_author_meta($td_social_id, $part_cur_auth_obj->ID);
                if (!empty($authorMeta)) {
                    echo td_social_icons::get_icon($authorMeta, $td_social_id, true );
                }
            }
            ?>
        </div>
    </div>
     
    <div class="profile-after"> 
    <!-- My channels---------------------------------------------->
    <div class="social-media">
      <?php global $wpdb;
            $id = $part_cur_auth_obj->ID; ?>
      <h6>My channels..</h6>
      <?php $var = "SELECT meta_value FROM $wpdb->usermeta WHERE user_id = $id AND meta_key= 'facebook_link'";
            $results = $wpdb->get_results( $var );
            if(!empty($results)) { ?>
            <span class="td-social-icon-wrap">
            <a target="_blank" href="<?php echo $results[0]->meta_value;?>" title="Facebook">
                <i class="td-icon-font td-icon-facebook"></i>
            </a>
        </span>
        <?php } else { ?><?php } ?>
        <?php $var1 = "SELECT meta_value FROM $wpdb->usermeta WHERE user_id = $id AND meta_key= 'twitter_link'";
            $results1 = $wpdb->get_results( $var1 );
            $link = $results1[0]->meta_value;
            if(!empty($link)) { ?>
        <span class="td-social-icon-wrap">
            <a target="_blank" href="<?php echo $results1[0]->meta_value;?>" title="Twitter">
                <i class="td-icon-font td-icon-twitter"></i>
            </a>
        </span>
        <?php } else { ?><?php } ?>
        <?php $var1 = "SELECT meta_value FROM $wpdb->usermeta WHERE user_id = $id AND meta_key= 'linkedin_link'";
            $results1 = $wpdb->get_results( $var1 );
            $link = $results1[0]->meta_value;
            if(!empty($link)) { ?>
        <span class="td-social-icon-wrap">
            <a target="_blank" href="<?php echo $results1[0]->meta_value;?>" title="linkedin">
                <i class="td-icon-font td-icon-linkedin"></i>
            </a>
        </span>
        <?php } else { ?><?php } ?>
        <?php $var1 = "SELECT meta_value FROM $wpdb->usermeta WHERE user_id = $id AND meta_key= 'youtube_link'";
            $results1 = $wpdb->get_results( $var1 );
            $link = $results1[0]->meta_value;
            if(!empty($link)) { ?>
        <span class="td-social-icon-wrap">
            <a target="_blank" href="<?php echo $results1[0]->meta_value;?>" title="youtube">
                <i class="td-icon-font td-icon-youtube"></i>
            </a>
        </span>
        <?php } else { ?><?php } ?>
        <?php $var1 = "SELECT meta_value FROM $wpdb->usermeta WHERE user_id = $id AND meta_key= 'website_link'";
            $results1 = $wpdb->get_results( $var1 );
            $link = $results1[0]->meta_value;
            if(!empty($link)) { ?>
        <span class="td-social-icon-wrap">
            <a target="_blank" href="<?php echo $results1[0]->meta_value;?>" title="website">
                <i class="td-icon-font td-icon-home"></i>
            </a>
        </span>
        <?php } else { ?><?php } ?>
    </div>
   <!-- My channels End---------------------------------------------->
   
   <div class="map-live">
   <h5>I live in..</h5>
   <div id="map" class="location-map"></div>
   </div>
   
   
   <div class="send-sms">
    <a href="<?php echo get_home_url();?>/account/?section=message#/" class="td_ajax_load_more"> SEND ME A MESSAGE </i></a>
   </div>
   </div>
   
   <?php $var1 = "SELECT meta_value FROM $wpdb->usermeta WHERE user_id = $id AND meta_key= 'your_location'";
            $results1 = $wpdb->get_results( $var1 );
            $link = $results1[0]->meta_value; ?>
   <script>
	function myMap() {
	    var mapOptions = {
	        center: new google.maps.LatLng(<?php echo $results1[0]->meta_value;?>),
	        zoom: 10,
	        mapTypeId: google.maps.MapTypeId.MAP
	    }
	var map = new google.maps.Map(document.getElementById("map"), mapOptions);
	}
	</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDC_XVmMqrGf9klL43MYehDWcomSKXdSyY&callback=myMap"></script>
                               
    <div class="clearfix"></div>
</div>