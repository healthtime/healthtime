<?php
/*
*	Share post
*/
?>
<?php

$post_image_data = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID () ), 'carrie-blog-thumb');

if(has_post_thumbnail( get_the_ID () )) {
    $post_image = $post_image_data[0];
} else {
	$post_image = '';
}
?>
<div class="post-social-wrapper">
	<div class="post-social">
		<a title="<?php esc_html_e("Share this", 'carrie'); ?>" href="<?php the_permalink(); ?>" data-title="<?php the_title(); ?>" class="facebook-share"> <i class="fa fa-facebook"></i></a><a title="<?php esc_html_e("Tweet this", 'carrie'); ?>" href="<?php the_permalink(); ?>" data-title="<?php the_title(); ?>" class="twitter-share"> <i class="fa fa-twitter"></i></a><a title="<?php esc_html_e("Share with Google Plus", 'carrie'); ?>" href="<?php the_permalink(); ?>" data-title="<?php the_title(); ?>" class="googleplus-share"> <i class="fa fa-google-plus"></i></a><a title="<?php esc_html_e("Pin this", 'carrie'); ?>" href="<?php the_permalink(); ?>" data-title="<?php the_title(); ?>" data-image="<?php echo esc_attr($post_image); ?>" class="pinterest-share"> <i class="fa fa-pinterest"></i></a>
	</div>
	<div class="clear"></div>
</div>