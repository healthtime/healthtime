<?php
/**
 * WP Theme Header
 *
 * Displays all of the <head> section
 *
 * @package Carrie
 */
$carrie_theme_options = carrie_get_theme_options();

// Demo settings
if ( defined('DEMO_MODE') && isset($_GET['header_logo_position']) ) {
  $carrie_theme_options['header_logo_position'] = esc_html($_GET['header_logo_position']);
}
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<title><?php wp_title( '|', true, 'right' ); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta charset="<?php bloginfo( 'charset' ); ?>" />

<META NAME="ROBOTS" CONTENT="INDEX, FOLLOW">
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php wp_head(); ?>
<meta name="msvalidate.01" content="F6F41299DCD9FD57927160043E644D74" />
<meta name="google-site-verification" content="1RurZXi5dovUVpVzj3VLgJ6xc6AoJfzmhoHbtxrHEnI" />
<!-- up[date on 4/08/2017 -->
<script async src="http://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
 /* (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-9473415477449419",
    enable_page_level_ads: true
  });*/
</script>
<!-- End script -->


<!-- <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script> 
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-9473415477449419",
    enable_page_level_ads: true
  });
</script>-->

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-5503454413392101",
    enable_page_level_ads: true
  });
</script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-92184309-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-92184309-1');
</script>


</head>
<?php 
// Demo settings
if ( defined('DEMO_MODE') && isset($_GET['blog_layout']) ) {
  $carrie_theme_options['blog_layout'] = $_GET['blog_layout'];
}

?>
<body <?php body_class(); ?>>


<?php carrie_top_show(); ?>

<?php 

// Center logo
if(isset($carrie_theme_options['header_logo_position'])) {
  $header_container_add_class = ' header-logo-'.esc_attr($carrie_theme_options['header_logo_position']);
} else {
  $header_container_add_class = '';
}
?>
<?php 
// Disable header
if(!isset($carrie_theme_options['disable_header'])) {
  $carrie_theme_options['disable_header'] = false;
}

if(isset($carrie_theme_options['disable_header']) && !$carrie_theme_options['disable_header']):
?>
<header class="clearfix">
<div class="container<?php echo esc_attr($header_container_add_class); ?>">
  <div class="row">
    <div class="col-md-12">
     
      <div class="header-left">
        <?php carrie_header_left_show(); ?>
      </div>
      
      <div class="header-center">
        <?php carrie_header_center_show(); ?>
      </div>

      <div class="header-right">
        <?php carrie_header_right_show(); ?>
      </div>
    </div>
  </div>
    
</div>
<?php carrie_menu_below_header_show(); ?>
</header>
<?php endif; ?>
<?php 
// Site Header Banner
carrie_banner_show('below_header'); 
?>